var gulp = require('gulp'),
  sass = require('gulp-sass'),
  jade = require('gulp-jade'),
  browserSync = require('browser-sync'),
  changed = require('gulp-changed'),
  concat = require('gulp-concat'),
  reload = browserSync.reload;

gulp.task('sass', function () {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./public/assets/css/'))
    .pipe(reload({stream: true}));
});

gulp.task('jade2html', function () {
  gulp.src('./*.jade')
    .pipe(jade({pretty: true}))
    .pipe(gulp.dest('./public/'))
    .pipe(reload({stream: true}));
});

gulp.task('browser-sync', function () {
  browserSync({
    server: {
      baseDir: "./public",
      routes: {
        '/bower_components': './bower_components'
      }
    }
  });
});

gulp.task('copy:images', function () {
  return gulp.src('./assets/images/**/*')
    .pipe(changed('./public/assets/images/'))
    .pipe(gulp.dest('./public/assets/images/'))
    .pipe(reload({stream: true}));
});

gulp.task('copy:fonts', function () {
  return gulp.src(['./assets/fonts/*', './bower_components/bootstrap/fonts/*', './bower_components/font-awesome/fonts/*'])
    .pipe(gulp.dest('./public/assets/fonts/'))
});

gulp.task('copy:js', function () {
  return gulp.src('./assets/js/*')
    .pipe(gulp.dest('./public/assets/js/'));
});

gulp.task('concatCSS', function () {
  return gulp.src(['./bower_components/bootstrap/dist/css/bootstrap.css', 'bower_components/font-awesome/css/font-awesome.css', 'bower_components/animate.css/animate.css', './bower_components/flexslider/flexslider.css', './bower_components/pnotify/src/pnotify.core.css', './bower_components/select2/dist/css/select2.css'])
    .pipe(concat('assets.css'))
    .pipe(gulp.dest('./public/assets/css'));
});

gulp.task('concatJS', function () {
  return gulp.src(['./bower_components/jquery/dist/jquery.js', './bower_components/bootstrap/dist/js/bootstrap.js', './bower_components/flexslider/jquery.flexslider.js', './bower_components/select2/dist/js/select2.js', './bower_components/jquery.stellar/jquery.stellar.js', './bower_components/pnotify/src/pnotify.core.js'])
    .pipe(concat('assets.js'))
    .pipe(gulp.dest('./public/assets/js'));
});

gulp.task('default', ['copy:js', 'copy:images', 'copy:fonts', 'sass', 'concatCSS', 'concatJS', 'jade2html', 'browser-sync'], function () {
  gulp.watch("./assets/js/*.js", ['copy:js']);
  gulp.watch("./public/assets/js/*.js", ['concatJS']);
  gulp.watch('./assets/images/**/*', ['copy:images']);
  gulp.watch("./assets/scss/**/*.scss", ['sass']);
  gulp.watch("./public/assets/css/*.css", ['concatCSS']);
  gulp.watch("./*.jade", ['jade2html']);
});
